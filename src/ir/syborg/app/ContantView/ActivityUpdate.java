package ir.syborg.app.ContantView;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;


public class ActivityUpdate extends Activity {

    Bundle extras;


    @Override
    protected void onResume() {
        super.onResume();
        G.curentActivity = this;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        final EditText edt_update_family = (EditText) findViewById(R.id.edt_update_family);
        final EditText edt_update_name = (EditText) findViewById(R.id.edt_update_name);
        Button btn_updated = (Button) findViewById(R.id.btn_updated);

        extras = getIntent().getExtras();
        int position = 0;
        if (extras != null) {
            position = extras.getInt("Position_key");
        }

        final Input_filed updated_input_filed = G.input_record.get(position);
        edt_update_name.setText(updated_input_filed.input_filed_name);
        edt_update_family.setText(updated_input_filed.input_filed_family);

        btn_updated.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                updated_input_filed.input_filed_name = edt_update_name.getText().toString();
                updated_input_filed.input_filed_family = edt_update_family.getText().toString();
                finish();
            }
        });
    }
}