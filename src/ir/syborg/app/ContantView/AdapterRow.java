package ir.syborg.app.ContantView;

import java.util.ArrayList;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class AdapterRow extends ArrayAdapter<Input_filed> {

    public AdapterRow(ArrayList<Input_filed> array) {
        super(G.context, R.layout.list_view_row, array);

    }


    public static class ViewHolder {

        TextView     txt_name;
        TextView     txt_family;
        ImageView    img_profile;
        ImageView    img_delete;
        CheckBox     chk_accept;
        LinearLayout all_row;


        public ViewHolder(View convert_view) {

            txt_name = (TextView) convert_view.findViewById(R.id.txt_name);
            txt_family = (TextView) convert_view.findViewById(R.id.txt_family);
            img_profile = (ImageView) convert_view.findViewById(R.id.img_profile);
            img_delete = (ImageView) convert_view.findViewById(R.id.img_delete);
            chk_accept = (CheckBox) convert_view.findViewById(R.id.chk_accept);
            all_row = (LinearLayout) convert_view.findViewById(R.id.all_row);
        }


        public void fill(final ArrayAdapter<Input_filed> adapter, final Input_filed item, final int position) {
            txt_name.setText(item.input_filed_name);
            txt_family.setText(item.input_filed_family);
            img_profile.setImageResource(item.input_filed_image_profile);
            img_delete.setClickable(item.input_filed_image_delete);
            chk_accept.setChecked(item.input_filed_accept_check);

            img_delete.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    adapter.remove(item);

                }
            });

            all_row.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {

                    Intent intent = new Intent(G.curentActivity, ActivityUpdate.class);
                    intent.putExtra("Position_key", position);
                    G.curentActivity.startActivity(intent);

                }
            });

            chk_accept.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View view) {
                    CheckBox checked = (CheckBox) view;
                    item.input_filed_accept_check = checked.isChecked();
                    if (item.input_filed_accept_check) {
                        all_row.setBackgroundColor(Color.parseColor("#ff0000"));
                        txt_family.setTextColor(Color.parseColor("#0000ff"));
                    } else {
                        all_row.setBackgroundColor(Color.parseColor("#000000"));
                        txt_family.setTextColor(Color.parseColor("#ffffff"));
                    }
                }
            });

            if (item.input_filed_accept_check) {
                all_row.setBackgroundColor(Color.parseColor("#ff0000"));
                txt_family.setTextColor(Color.parseColor("#0000ff"));
            } else {
                all_row.setBackgroundColor(Color.parseColor("#000000"));
                txt_family.setTextColor(Color.parseColor("#ffffff"));
            }
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Input_filed item = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            convertView = G.inflater.inflate(R.layout.list_view_row, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.fill(this, item, position);

        return convertView;
    }
}
