package ir.syborg.app.ContantView;

import java.util.Random;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class ActivityContantView extends Activity {

    int          img_res_id;
    ArrayAdapter adapter;


    @Override
    protected void onResume() {
        super.onResume();

        G.curentActivity = this;
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lst_show_record = (ListView) findViewById(R.id.lst_show_record);

        adapter = new AdapterRow(G.input_record);
        lst_show_record.setAdapter(adapter);

        for (int i = 0; i < 30; i++) {
            final int random_int = new Random().nextInt((6 - 1) + 1) + 1;
            String img_name = "profile_0" + String.valueOf(random_int);
            img_res_id = getResources().getIdentifier(img_name, "drawable", getPackageName());
            Input_filed inputs = new Input_filed();

            inputs.input_filed_name = "Name " + (i + 1);
            inputs.input_filed_family = "Family " + (i + 1);
            inputs.input_filed_image_delete = false;
            inputs.input_filed_image_profile = img_res_id;

            G.input_record.add(inputs);
        }
        adapter.notifyDataSetChanged();
    }
}